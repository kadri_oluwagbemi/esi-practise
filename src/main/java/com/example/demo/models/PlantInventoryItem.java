package com.example.demo.models;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data

public class PlantInventoryItem {
    @Id
    @GeneratedValue //They both come from JPA


    Long id;
    String serialNumber;

    @Enumerated(EnumType.STRING)
    EquipmentCondition equipmentCondition;

    @ManyToOne
    PlantInventoryEntry plantInfo;

}
