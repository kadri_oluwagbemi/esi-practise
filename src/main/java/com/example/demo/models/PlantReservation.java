package com.example.demo.models;

import lombok.Data;
import lombok.Generated;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
public class PlantReservation {
    @Id @GeneratedValue

    Long id;

    @Embedded
    BusinessPeriod schedule;

    @ManyToOne
    PlantInventoryItem plant;

    @ManyToOne
    MaintenancePlan maintain;

    LocalDate startDate;
    LocalDate endDate;
}
