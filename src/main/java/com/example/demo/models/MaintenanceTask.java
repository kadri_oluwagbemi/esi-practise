package com.example.demo.models;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;


@Entity
@Data

public class MaintenanceTask {
    @Id
    @GeneratedValue

    String description;

    @Enumerated(EnumType.STRING)
    TypeOfWork typeOfWork;

    BigDecimal price;

    @Embedded
    BusinessPeriod schedule;

    @ManyToOne
    PlantReservation reservationinfo;
}

