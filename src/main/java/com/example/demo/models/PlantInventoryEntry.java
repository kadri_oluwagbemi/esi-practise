package com.example.demo.models;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;

//Information that will be stored in the database

//We will use Java persistence API

@Entity
@Data
public class PlantInventoryEntry {
    @Id
    @GeneratedValue //They both come from JPA

    Long id;
    String name;
    String description;

    BigDecimal price;
}
