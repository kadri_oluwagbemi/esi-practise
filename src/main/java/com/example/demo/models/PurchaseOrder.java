package com.example.demo.models;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Data
public class PurchaseOrder {
    @Id @GeneratedValue
    Long id;

    @Column(precision=8,scale=2)
    BigDecimal total;

    @Enumerated(EnumType.STRING)
    POStatus status;

    @Embedded
    BusinessPeriod schedule;

    @ManyToOne
    PlantInventoryEntry plantinfo;
}
