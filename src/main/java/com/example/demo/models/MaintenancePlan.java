package com.example.demo.models;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class MaintenancePlan {
    @Id
    @GeneratedValue

    int yearOfAction;

    @OneToMany(cascade={CascadeType.ALL})
    List<MaintenanceTask> getTasks;

    @ManyToOne
    PlantInventoryItem plant;


}
