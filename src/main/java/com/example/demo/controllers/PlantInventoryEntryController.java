package com.example.demo.controllers;

import com.example.demo.repository.InventoryRepository;
import com.example.demo.models.PlantInventoryEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

//@RestController //Controller is used when we will want to return HTML.
//@RestController  // will return a JSON file
@RestController
public class PlantInventoryEntryController {
    @Autowired
    InventoryRepository repo; //Access to repository

    @GetMapping("/")
    public List<PlantInventoryEntry> findAll() {
        return repo.findAvailable("excavator", LocalDate.now(), LocalDate.now());
    }

    @GetMapping("/plant")
    public List<PlantInventoryEntry> findByNameContaining() {
        return repo.findByNameContaining("excavator");
    }
}

    /*@GetMapping("/plant")
    public List<PlantInventoryEntry> findAll(){
        return repo.findAll();
    }*/

   /* @GetMapping("/")
    public String list(Model model) {
    model.addAttribute("plants", repo.findAll());
    return "plants/list";
    }

    @GetMapping(value="/plants/form")
    public String form(Model model){
        model.addAttribute("plant", new PlantInventoryEntry());
        return "plants/create";
    }

    @PostMapping(value="/")
    public String create(PlantInventoryEntry plant){
        repo.save(plant);
        return "redirect:/plants";*/
