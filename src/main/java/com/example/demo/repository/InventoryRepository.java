package com.example.demo.repository;

import com.example.demo.models.PlantInventoryEntry;
import com.example.demo.repository.CustomInventoryRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface InventoryRepository extends JpaRepository<PlantInventoryEntry, Long>, CustomInventoryRepository {

}
