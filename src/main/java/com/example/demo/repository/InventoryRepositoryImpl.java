package com.example.demo.repository;

import com.example.demo.models.PlantInventoryEntry;
import com.example.demo.repository.CustomInventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.List;

public class InventoryRepositoryImpl implements CustomInventoryRepository {
    @Autowired
    EntityManager em; /*entitymanager can be used to create query*/

    @Override
    public List<PlantInventoryEntry> findAvailable(String excavator, LocalDate from, LocalDate to) {
        return em.createQuery("select p from PlantInventoryEntry where LOWER(p.name) like ?1", PlantInventoryEntry.class)
                .setParameter(1, excavator)
                .getResultList();
    }

    @Override
    public List<PlantInventoryEntry> findByNameContaining(String excavator) {
        return em.createQuery("select p from PlantInventoryEntry", PlantInventoryEntry.class)
                .getResultList();
    }

}
