package com.example.demo.repository;

import com.example.demo.models.PlantInventoryEntry;
import com.example.demo.models.PlantInventoryItem;
import com.example.demo.models.PlantsWithCount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface PlantInventoryEntryRepository extends JpaRepository <PlantInventoryEntry, Long> {
    List<PlantInventoryEntry> findByNameContaining(String str);
    @Query("select p from PlantInventoryEntry p where LOWER(p.name) like ?1")

    List<PlantsWithCount> findAvailable(String excavator, LocalDate from, LocalDate to);

    PlantInventoryItem findOneByPlantInfo(PlantInventoryEntry entry);
}
