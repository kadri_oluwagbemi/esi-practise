package com.example.demo.repository;

import com.example.demo.models.MaintenancePlan;
import com.example.demo.models.PlantInventoryItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface MaintenancePlanRepository extends JpaRepository <MaintenancePlan, Long>{

    List<MaintenancePlan> findCorrectiveRepairsByYearForPeriod(int i, int thisYear);
}
