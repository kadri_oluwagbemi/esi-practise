package com.example.demo.repository;

import com.example.demo.models.PlantInventoryEntry;
import com.example.demo.models.PlantInventoryItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface PlantInventoryItemRepository extends JpaRepository<PlantInventoryItem, Long> {
    List<PlantInventoryItem> findByPlantInfo(PlantInventoryEntry entry);

    List<PlantInventoryItem> findPlantsNotHiredForPeriod(LocalDate start, LocalDate end);
}
