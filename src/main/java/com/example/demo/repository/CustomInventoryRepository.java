package com.example.demo.repository;

import com.example.demo.models.PlantInventoryEntry;
import com.example.demo.models.PlantsWithCount;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;

public interface CustomInventoryRepository {
    List<PlantInventoryEntry> findAvailable(String excavator, LocalDate from, LocalDate to);

    List<PlantInventoryEntry> findByNameContaining(String str);
}
